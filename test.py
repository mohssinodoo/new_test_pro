def foo(arg1, arg2):
    """
    A method's docstring with parameters and return value.
    
    Use all the cool Sphinx capabilities in this description, e.g. to give
    usage examples ...
    
    :Example:
 
    >>> another_class.foo('', AClass())        
        
    :param arg1: first argument
    :type arg1: string
    :param arg2: second argument
    :type arg2: :class:`module.AClass`
    :return: something
    :rtype: string
    :raises: TypeError
    """
        
        return '' + 1

