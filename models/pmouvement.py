# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    @api.onchange('mobile')
    def change_stuff(self):
        self.ensure_one()
        self.x_studio_field_KtAIi.strip()
        raise Warning(self.x_studio_field_KtAIi)

class Pmouvement(models.Model):
    _name = 'teos.pmouvement'

    id_movimiento = fields.Char(string='id mouvement interne', size=24, required=True)
    apunte = fields.Char(string='ecriture', size=10, required=True)
    anio = fields.Char(string='an de la facture', size=5, required=True)
    cod_origen = fields.Char(string='code origine', size=16, required=True)
    cod_diario = fields.Char(string='code journal de compta', size=5, required=True)
    cod_empresa = fields.Char(string='code entreprise', size=8, required=True)
    linea = fields.Char(string='ligne compter a chaque ecriture', size=100, required=True)
    cod_grp_cod = fields.Char(string='groupe des codes', size=1, default='N', required=True)
    cod_tercero = fields.Char(string='code client fournisseur', size=10, required=True)
    f_contable = fields.Char(string='date comptable de l ecriture', size=10, required=True)
    asiento = fields.Char(string='code asiento', size=8, required=True)
    x_aux = fields.Char(string='code complete comte aux', size=24, required=True)
    x_contrapartida = fields.Char(string='compte contrepartie', size=10, required=True)
    cod_pgc = fields.Char(string='code plan comptable', size=5, required=True)
    c_importe1 = fields.Char(string='montant divise1 de l entprs', size=16, required=True)
    c_importe2 = fields.Char(string='montant divise2 de l entprs', size=5, required=True)
    ck_signo = fields.Char(size=8, required=True)
    concepto = fields.Char(size=100, required=True)
    id_fact_prev = fields.Char(size=1, default='N', required=True)
    f_valor = fields.Char(size=10, required=True)
    ck_concil_banco = fields.Char(size=10, required=True)
    documento = fields.Char(size=8, required=True)
    id_doc_punteo = fields.Char(size=10, required=True)
    ck_visible = fields.Char(size=5, required=True)
    ck_tipo = fields.Char(size=16, required=True)
    num_doc = fields.Char(size=5, required=True)
    num_doc_impreso = fields.Char(size=8, required=True)
    id_conciliacion = fields.Char(size=100, required=True)
    x_origen_diario = fields.Char(size=1, default='N', required=True)
    x_subcta = fields.Char(size=10, required=True)
    x_cod_aux = fields.Char(size=10, required=True)
    cod_gto_fin = fields.Char(size=8, required=True)
    ck_tipo_prevision = fields.Char(size=10, required=True)
